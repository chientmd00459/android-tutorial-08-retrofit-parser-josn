package com.example.android8tutorial.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
