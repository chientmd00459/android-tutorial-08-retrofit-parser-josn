package com.example.android8tutorial.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android8tutorial.R;
import com.example.android8tutorial.adapter.NewsAdapter;
import com.example.android8tutorial.interfaces.NewsOnClick;
import com.example.android8tutorial.model.Item;

public class ListNewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);

        getData();

        adapter = new NewsAdapter(this,listDatas());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);

        RecyclerView rvNews = findViewById(R.id.rvNews);
        rvNews.setLayoutManager(layoutManager);
        rvNews.setAdapter(adapter);

        adapter.setiOnClick(new NewsOnClick(){
            @Override
            public void onClickItem(int position) {
                Item model = listDatas.get(position);
                Intent intent = new Intent(ListNewsActivity.this,DetailActivity.class);
                intent.putExtra("URL", model.getContent().getUrl());
                startActivity(intent);
            }
        })
    }
}
